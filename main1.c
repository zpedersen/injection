//Command Line Interpreter
//Zach Pedersen, Justin Dietrich
//Prof. Citro
//CST-315

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>

//getline declarations
char *input = NULL;
	size_t capline = 0;
int i;
	char *token;
	char *array[256]; 

bool quit = false;

// User prompts
void startDisplay(){
	printf("Command Line Interpreter\n");
	printf("Run a command by typing and pressing Enter\n");
	printf("To exit, type 'quit'\n");
}

// Command Input Prompt
void inputPrompt(){
	printf("Command: ");
}

// Function to trim white space from Adam Rosenfield StackOverflow
char *trim(char *str)
{
	char *end;

	// Trim leading space
	while(isspace((unsigned char)*str)) str++;

	if(*str == 0)  // All spaces?
		return str;

	// Trim trailing space
	end = str + strlen(str) - 1;
	while(end > str && isspace((unsigned char)*end)) end--;
	// Write new null terminator character
	end[1] = '\0';

	return str;
}

//Command Execution
//Creates new process, waits for termination
void execute(){
	int pid = fork();
		if (pid != 0) {
			int s;
			waitpid(-1, &s, 0);
			} 

    //For invalid commands
    else {
		//printf("(%s)\n", trim(array[0]));
			if(execvp(trim(array[0]), array) == -1){
			  perror("Invalid command");
					exit(errno);
				}				
	  }
}

void execute2()
{
	int pid = fork();
		if (pid != 0) {
			int s;
			waitpid(-1, &s, 0);
			} 

    //For invalid commands
    else {
		//printf("(%s)\n", trim(array[0]));
			if(system(trim(array[0])) == -1){
			  perror("Invalid command");
					exit(errno);
				}
				else {
					exit(0);
				}				
	  }
}

//Divide into tokens
//Add tokens into array
//*
void makeTokens(char *input){
	const char d[2] = ";\n";
	i = 0;
		token = strtok(input, d);
			while (token != NULL) { 
				array[i] = token;
				//printf("(%s)\n", token);
				if (strcmp(array[0], "quit") == 0) {
					printf("Shell Terminated\n");
					quit = true;
					break;
					//exit(0);
					//return 0;
				}
				else{
					execute2();
					token = strtok(NULL, d);
				}
				
				
			}
		array[i] = NULL;
}




//*
int main(int argc, char *argv[]){
	if (argc >= 2 && strlen(argv[1]) != 0)
	{
		//printf("%s", argv[1]);
		//strcpy(input, *argv);
		makeTokens(argv[1]);
	}
	startDisplay();
		while(quit == false){
			inputPrompt(); //Displays prompt
			getline(&input, &capline, stdin); //Reads input
		if(strcmp(input,"\n")==0){ //Checks if input is empty
			perror("Enter a command " );
				continue;
				}
	    makeTokens(input);
		}
	return 0;
}
