Justin Dietrich
Zach Pedersen

To do this program, you are first going to have to compile all of the c files.
Download all of the files into the same directory, navigate to that directory in the terminal, then enter these commands.

gcc -o virus virus.c
gcc -o scanner ScanDir.c
gcc -pthread main1.c -o shell

After that, run the virus with this command.

./virus

The virus is disguised as the shell, but has actuall run your shell with an order to mine bitcoin on your computer!
You will see that the virus has mined 1 bitcoin in the directory that it is installed in! (1bitcoin.txt)
To scan for what files are causing this, run the scanner.

./scanner

It should say that the virus file and executable are the culprits!