//Command Line Interpreter
//Zach Pedersen, Justin Dietrich
//Prof. Citro
//CST-315
// Code to open each file in a directory from 
// https://www.decodeschool.com/C-Programming/File-Operations/C-Program-to-read-all-the-files-located-in-the-specific-directory

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
int main(void)
{
    DIR *d;
    struct dirent *dir;
    FILE *fptr;
    char ch;
    char string[255];
    int found = 0;

    d = opendir("./");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
        	
	    	fptr = fopen(dir->d_name, "r");
		if (fptr != NULL)
    		{
			while (fscanf(fptr, " %256s", string) == 1)
            {
                //printf("%s\n", string);
                if(strlen(string) >= 12 && strncmp(string, "1bitcoin.txt", 12) == 0)
                {
                    printf("%s has been mining bitcoins behind your back!\n", dir->d_name);
                    found += 1;
                }
            }
    			fclose(fptr);
        	}
		
        }
        closedir(d);
        if (found <= 0)
        {
            printf("All files are clear!\n");
        }
    }
    return(0);
}